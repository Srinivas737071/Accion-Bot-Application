const express = require("express");
const app = express();
const http = require("https");
let mysql = require("mysql");
let fs = require("fs");

var con = mysql.createConnection({
  host: "localhost", //"intranet.accionlabs.com",
  port: "3306",
  user: "root",
  password: "admin",
  database: "AccionEmployeeDb"
});

con.connect(function(err) {
  if (err) {
    throw err;
  }
  console.log("Connected!");
  var sqltable =
    "CREATE TABLE IF NOT EXISTS Employee ( EmailID VARCHAR(255) UNIQUE,Date_of_birth VARCHAR(255) , Marital_Status VARCHAR(255), Gender VARCHAR(255) , Department VARCHAR(255), Reporting_To VARCHAR(255), Employeestatus VARCHAR(255), Role VARCHAR(255), Experience VARCHAR(255), Employee_type VARCHAR(255), Business_HR VARCHAR(255), FirstName VARCHAR(255), LastName VARCHAR(255), EmployeeID VARCHAR(255) UNIQUE, Dateofexit VARCHAR(255), Other_Email VARCHAR(255), Work_location VARCHAR(255), LocationName VARCHAR(255), Designation VARCHAR(255), Dateofjoining VARCHAR(255), Birth_Date_as_per_Records VARCHAR(255), Work_phone VARCHAR(255),Photo VARCHAR(255))";
  con.query(sqltable, function(err, result) {
    if (err) throw err;
    console.log("Table created");
  });
});

app.get("/", (req, res) => {
  res.send("Accion Bot Application");
});

let count = 0;
var realEmpObj = {};
var updateIndex = 0;
function getRequest(index) {
  updateIndex = index + 200;
  var url =
    "https://people.zoho.com/people/api/forms/employee/getRecords?authtoken=431fce4ebc4618dfc90d7c114f5e00f7&sIndex=" +
    index +
    "&limit=200";
  var request = http.request(url, function(res) {
    let data = "";
    res.on("data", function(chunk) {
      data += chunk;
    });
    res.on("end", function() {
      if (JSON.parse(data).response.result) {
        const emps = JSON.parse(data).response.result;
        // add your db logic here
        emps.forEach(emp => {
          Object.keys(emp).forEach(key => {
            realEmpObj = emp[key][0];
            var sql = `INSERT INTO Employee (EmailID,
                Date_of_birth,
                Marital_Status,
                Gender,
                Department,
                Reporting_To,
                Employeestatus,
                Role,
                Experience,
                Employee_type,
                Business_HR,
                FirstName,
                LastName,
                EmployeeID,
                Dateofexit,
                Other_Email,
                Work_location,
                LocationName,
                Designation,
                Dateofjoining,
                Birth_Date_as_per_Records,
                Work_phone,
                Photo) VALUES (?) ON DUPLICATE KEY UPDATE Date_of_birth=values(Date_of_birth),
                Marital_status=values(Marital_status),
                Gender=values(Gender),
                Department=values(Department),
                Reporting_To=values(Reporting_To),
                Employeestatus=values(Employeestatus),
                Role=values(Role),
                Experience=values(Experience),
                Employee_type=values(Employee_type),
                Business_HR=values(Business_HR),
                FirstName=values(FirstName),
                LastName=values(LastName),
                EmployeeID=values(EmployeeID),
                Dateofexit=values(Dateofexit),
                Other_Email=values(Other_Email),
                Work_location=values(Work_location),
                LocationName=values(LocationName),
                Designation=values(Designation),
                Dateofjoining=values(Dateofjoining),
                Birth_Date_as_per_Records=values(Birth_Date_as_per_Records),
                Work_phone=values(Work_phone),
                Photo=values(Photo)`;
            let insertArr = [
              realEmpObj.EmailID,
              realEmpObj.Date_of_birth,
              realEmpObj.Marital_status,
              realEmpObj.Gender,
              realEmpObj.Department,
              realEmpObj.Reporting_To,
              realEmpObj.Employeestatus,
              realEmpObj.Role,
              realEmpObj.Experience,
              realEmpObj.Employee_type,
              realEmpObj.Business_HR,
              realEmpObj.FirstName,
              realEmpObj.LastName,
              realEmpObj.EmployeeID,
              realEmpObj.Dateofexit,
              realEmpObj.Other_Email,
              realEmpObj.Work_location,
              realEmpObj.LocationName,
              realEmpObj.Designation,
              realEmpObj.Dateofjoining,
              realEmpObj.Birth_Date_as_per_Records,
              realEmpObj.Work_phone,
              realEmpObj.Photo
            ];
            sql = mysql.format(sql, [insertArr]);
            con.query(sql, function(err, result) {
              if (err) console.log(err);
              if(result){
               count++;
               return console.log("data updated count: ",count);
              }
              console.log("data adding");
            });
          });
        });
      } else if (JSON.parse(data).response.status == 1) {        
        switch (JSON.parse(data).response.errors.message) {
          case "No records found":
            con.query("Select count(*) from employee", (e, r) => {
              if(e) console.log(e);
              if(r[0]["count(*)"]+1 == count) {
                fs.writeFile("update.txt", `Passed ${count} data on ${Date()}`, err => {
                  if (err) throw err;
                  else {
                    console.log(`Succesfully update ${count} data and exiting the process`);
                    process.exit();
                  }
                });
              }
            })
            break;
          default:
            fs.writeFile("update.txt", `Fail: ${JSON.parse(data).response.errors.message} on ${Date()}`, err => {
              if (err) throw err;
              else {
                console.log("Updated ${count} data Exiting the process with error");
                process.exit();
              }
            });
            break;
        }
      }
      getRequest(updateIndex);
    });
  });
  request.on("error", function(e) {
    console.log(e.message);
  });
  request.end();
}

getRequest(updateIndex);


app.listen(3001, () => {
  console.log("Server started");
});
